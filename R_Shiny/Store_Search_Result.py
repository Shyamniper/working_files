# librarires
import json
import random
import pandas as pd
import csv
import time
import string
import requests
import preprocessor as p
def creat_files(SEARCH_TEXT,FROMDATE,TODATE):
	tweets_data = []
	with open('%s.json' % (SEARCH_TEXT),'r+') as f:
		for line in f.readlines():
			try:
				tweets=json.loads(line)
				tweets_data.append(tweets)
			except:
					continue
				#Create a function to see if the tweet is a retweet
			def is_RT(tweet):
					if 'retweeted_status' not in tweet:
						return False      
					else:
						return True
				##Create a function to see if the tweet is a reply to a tweet of #another user, if so return said user
			def is_Reply_to(tweet):
					if 'in_reply_to_screen_name' not in tweet:
						return False      
					else:
						return tweet['in_reply_to_screen_name']
	# ###add col to dataframe
	df=pd.DataFrame()
	df['ID'] = list(map(lambda tweets_data:tweets_data['id'], tweets_data))
	df['created_at'] = list(map(lambda tweet: time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(tweet['created_at'],'%a %b %d %H:%M:%S +0000 %Y')), tweets_data))
	df['text'] = list(map(lambda tweets_data: tweets_data['text'] if 'extended_tweet' not in tweets_data else tweets_data['extended_tweet']['full_text'], tweets_data))
	df['lang'] = list(map(lambda tweets_data:tweets_data['lang'], tweets_data))
	df['hashtags'] = list(map(lambda tweets_data:tweets_data['entities']['hashtags'], tweets_data))
	df['mentions'] = list(map(lambda tweets_data:tweets_data['entities']['user_mentions'], tweets_data))
	df['user'] = list(map(lambda tweets_data:tweets_data['user']['screen_name'], tweets_data))
	df['location'] = list(map(lambda tweets_data:tweets_data['user']['location'], tweets_data))
	df['favorite_count'] = list(map(lambda tweets_data:tweets_data['favorite_count'], tweets_data))
	df['retweet_count'] = list(map(lambda tweets_data:tweets_data['retweet_count'], tweets_data))
	df['country_code'] = list(map(lambda tweets_data: tweets_data['place']['country_code'] if tweets_data['place']!= None else '', tweets_data))
	df['coordinates'] = list(map(lambda tweets_data: tweets_data['coordinates']['coordinates'] if tweets_data['coordinates']!= None else '', tweets_data))
	df['favorite_count'] = df['favorite_count']
	df['retweet_count'] = df['retweet_count']
	df['RT'] = list(map(is_RT, tweets_data))
	df['Reply'] = list(map(is_Reply_to, tweets_data))
	df.to_csv('tweets_%s_%s_%s.csv' % (SEARCH_TEXT,FROMDATE,TODATE), encoding='utf-8', index=False)
if __name__ == '__main__':
 creat_files()
